// Using let in ES6 
function foo(){
var a = 1;
if (a>=0){
console.log(a);
let b=2;
const myConst = "some-value";
}
console.log(b);
console.log(myConst);
}

// Arrow Functions
var multiply =(x,y) =>{return x*y}

// New functions in Math object 
Math.sign(-11); // -1
Math.sign(7); // 1

//Spread Operator
let fruits = ['banana'];
let moreFruits = ['apple','orange'];
let someMoreFruits = [...fruits,...moreFruits];
console.log(someMoreFruits);

// Parameters 
function multi(a, b= 5) {
   return a * b;
}

multi(3); // 15
multi(2, undefined); // 10
multi(5, 4); // 20

// Creating classes
class Animal {
   constructor(name) {
      this.name = name;
      this.type = 'animal';
   }
   getName() {
      return this.name;
   }   
}

// Create an instance
let someAnimal = new Animal('dog');
console.log(someAnimal);

// Using Sympbols
var mode;
var prod= Symbol("productions");
var dev = Symbol("Development")
mode= prod;
switch(mode){
 case dev:
 console.log("Dev Server");
// Development Server
case prod:
// production server
console.log("Prod Server");
}

// using Modules
// import { multiply, myConst } from "lib/compute.js";
// console.log(multiply(5,myConst));