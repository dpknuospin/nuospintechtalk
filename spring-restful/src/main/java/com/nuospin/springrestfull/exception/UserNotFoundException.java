package com.nuospin.springrestfull.exception;

public class UserNotFoundException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2375223598593719804L;
	private static final String DEFAULT_MESSAGE = "User not found !";


	public UserNotFoundException() {
		this( DEFAULT_MESSAGE);
	}



	public UserNotFoundException(String message) {
		super(message);
		message  = DEFAULT_MESSAGE;
		
	}

}
