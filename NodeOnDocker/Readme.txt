Before you run this project, ensure the following is installed to your system:

1.) NodeJS  
2.) Docker 
    Mac users follow this link : https://docs.docker.com/engine/installation/mac/
    Ubuntu users follow this link: https://docs.docker.com/engine/installation/linux/ubuntulinux/
    Windows users follow this link: https://docs.docker.com/engine/installation/windows/

Now, To run this application, Traverse to directry "NuospinTechTalk / NodeOnDocker / src" and type "npm install" and hit enter. This command  
will download all depedencies. 

Now follow the steps:

1.) Make sure Docker-Deamon is running
   
    "$ sudo status docker"

    if docker deamon is stop , then run following command:
  
    "$ sudo start docker"

2)  Create Image:

    Docker file "Dockerfile" is already created in project directory , so now to create image for our application use command:

    "$ docker build -t <your username>/node_on_docker ."

    It will take few seconds and after completion you will notify by Success message. For example:

    Successfully built <build_id>

    To check is your image has been created or not use following command:

    "$ docker images"

    This will list down all your docker images 

    # Example
    REPOSITORY                      TAG        ID              CREATED
    node                           argon      479c0211cd76    3 weeks ago
   <your username>/node_on_docker  latest     f64d3505b0d2    1 minute ago

3) Run Image:

   To run image "<your username>/node_on_docker" use follwoing command:

   "$ docker run -p 49160:8080 -d <your username>/node_on_docker"

   or

   "$ docker run -p 49160:8080 -d <image_id>"
   here "image_id" you can get using "docker images" command. Example: f64d3505b0d2 

   After running above command a long string will be printed on terminal like:

   "ca2109cee29a05112a8d565ec0f38c626f5f4d1b61e7aa50cda2e15250026c7e"

   To check is your Image is running inside container you can list down all container using following command:

   "$ docker ps"

   Output will printed on your terminal like:

   CONTAINER ID        IMAGE                  COMMAND            CREATED             STATUS              PORTS                      NAMES
   250c95096dd3   satwinder/node_on_docker   "npm start"     2 minutes ago       Up 2 minutes   0.0.0.0:49160->8080/tcp    nostalgic_hawking


   You can see there a container named "satwinder/node_on_docker" with id "250c95096dd3" has been created by running image.
  
   To see logs you can use following command 
   
   "$ docker logs <conatiner_id>"

   example: Running on http://localhost:8080

4) Test
  
   Now you can call your app using curl (install if needed via: "sudo apt-get install curl"):

   "$ curl -i localhost:46160"

   or

   "Hit http://localhost:46160"

5) Done.


For more information visit: http://blog.nuospin.com/dockerizing-your-node-applications



