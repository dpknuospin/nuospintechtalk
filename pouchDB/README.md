##pouchdb-getting-started-todo
============================

#The Git repository for the getting started tutorial for pouchdb-getting-started-todo
============================



**EXERCISE ONE:**

 
* Downloading prerequisites for PouchDB

1. Visit <https://pouchdb.com/getting-started.html> and download the .zip file which contains the basic template for the todo project which works on todoMVC.
2. Change your working directory to unzipped pouchdb-getting-started-todo as: `$ cd pouchdb-getting-started-todo>`
3. Using python start a http server by running the command : `$ python -m SimpleHTTPServer` ( make sure you have python installed on your computer, check by running command on terminal :  $ python  -v)
  
  Check if you are on right track by visit localhost:8000 in browser.

 
**EXERCISE TWO:**
 
* Installing PouchDB in your app
 
 
1. Open index.html in Sublime Text editor or TextMate and include PouchDB in the app by adding a script tag  : 
  `<script src= "//cdn.jsdelivr.net/pouchdb.6.0.5/pouchdb.min.js" ></script>`
 

**EXERCISE THREE:**

* Create a database for attendance  :

 1. This is done by simply instantiating a new PouchDB object with the name of the database in the app.js file as :
 `var db = new PouchDB('presentdb');`

 
**EXERCISE FOUR**
 
1. Write operation on database: addTodo function is called with text being passes as the user presses Enter key.  (Shown with code app.js , discuss role of _id field, use of promise instead of callback)

2. Read operation or show items from database : to read all documents `db.allDocs()` is used and include_docs option tells PouchDB to give the data within each document , results can be ordered based on their _id field.

3. Function  `checkboxChanged()` is called for editing a todo n calls db.put . (discuss similarity with creation of the document but with addition of _rev field)

4. To delete an object call to `db.remove()` is made with that very object (shown with code)


**EXERCISE FIVE:**

1. Updating the UI : in PouchDB since u may be syncing data remotely, we have to be sure update occurs no matter from where changes come from.
  `db.changes()` handles the updates to the database .(shown in code app.js , edit between remoteCouch and addTodo)


**Exercise Six:**

1. Installing CouchDB : to implement syncing we need a compatible server , we can either install PouchDB-Server or CouchDB. To install CouchDB run at terminal : `$ sudo apt-get install couchdB`  (CouchDB runs at port 5984)
  If installed locally check if everything works by opening : <http://localhost:5984/_utils>


2. Enabling Cross Origin Resource Sharing : we need it because CouchDB is hosted on one port and application is hosted on another. When we need to fetch resource from one server or the other CORS is responsible for this.
  To install CORS globally, run this on terminal : 

  `$ npm install -g add-cors-to-couchdb`
  `$ add-cors-to-couchdb`

 To check everything is working open browser and visit  : <http://localhost:5984/_utils/config.html>  (cors column should have credentials set to True)

3. Two Way Sync : with CORS enabled , we have to edit our remoteCouch as :
  `var remoteCouch = 'localhost:5984/attendance' ;`
  Now we can implement sync function ( Shown in code function sync ) .
  `db.replicate()` asks PouchDB to sync all the documents to and from the remoteCouch.

Links :
[PouchDB](https://pouchdb.com/) [CouchDB](http://couchdb.apache.org/)

