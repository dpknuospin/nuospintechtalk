package com.nuospin.springrestfull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.nuospin.springrestfull.domain.User;
import com.nuospin.springrestfull.dto.UserDto;
import com.nuospin.springrestfull.service.UserService;

public class UserServiceTest extends ApplicationTest {
	
	@Autowired
	private UserService userService;

	@Test
	public void shouldBeableToSaveUser(){
		UserDto user = buildUser();
		User registeredUser = userService.saveUser(user);
		System.out.println("user" + registeredUser);
		Assert.notNull(registeredUser);
		Assert.isTrue(registeredUser.getEmail().equals("pooja@nuospin.com"));

	}
	


	private UserDto buildUser() {
		UserDto user = new UserDto();
		user.setEmail("pooja@nuospin.com");
		user.setFirstName("pooja");
		user.setLastName("mehra");
		return user;

	}

}