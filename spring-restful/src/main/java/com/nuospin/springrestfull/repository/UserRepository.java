package com.nuospin.springrestfull.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nuospin.springrestfull.domain.User;

public interface UserRepository extends MongoRepository<User, String> {
	
	 User findByEmail(String email);
	 User findByfirstName(String firstName);

}
