package com.nuospin.springrestfull.dto;

import java.io.Serializable;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

public class UserDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8554005979609684451L;

	private String id;

	@NotBlank
	@Email
	private String email;

	private String firstName;

	private String lastName;

	private String contactNumber;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	@Override
	public String toString() {
		return "UserDto [id=" + id + ", email=" + email + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", contactNumber=" + contactNumber + "]";
	}

}
