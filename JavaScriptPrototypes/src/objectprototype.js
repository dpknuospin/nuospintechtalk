var employee = {
    "firstName": "Sachin"
};

console.log("employee name before adding properties", employee); // firstName:Sachin

Object.defineProperty(employee, 'firstName', {
    writable: false,
    enumerable: true,
    configurable: false
});

console.log("Now the employee is reffering to", employee); //it is reffering to sachin
employee.firstName = "Rahul";
console.log("After changes employee first name changes to", employee); // employee firstName doesn't changes as writable is set to false ,so firstName is still sachin 
delete employee.firstName;
console.log("employee after firstname deletion", employee); //Unable to delete firstName as configurable is set to false

//define a config variable that holds the value of all the properties of 
//Object.prototype
var config = {
    writable: true,
    enumerable: true,
    configurable: true

};

var defineProperty = function(obj, name, value) {
    config.value = value;
    Object.defineProperty(obj, name, config);
}

var employee = {};
    defineProperty(employee, 'firstName', "Shashank");
    defineProperty(employee, 'lastName', "Singh");
    defineProperty(employee, "age", 23);
console.log("Employee is", employee); //print the employee object having name 
//as shashank singh and age is 23

employee.firstName = "Satwinder";
    delete employee.age;

console.log("employee after modification", employee); //changes the employee 
//object firstName to Satwinder and also deletes the age as writable and 
//configurable properties are set to TRUE

//Enumerable property
var sample = {
    a: 1,
    b: 2
};

sample.c = 3;
Object.defineProperty(sample, 'd', {
    value: 4,
    enumerable: false
});

sample.d; // => 4

for (var key in sample) console.log(sample[key]);
// Console will print out
// 1
// 2
// 3
// Here d property of sample object can't be exposed by the for loop as 
//enumerable is set to false

Object.keys(sample); // => ["a", "b", "c"]