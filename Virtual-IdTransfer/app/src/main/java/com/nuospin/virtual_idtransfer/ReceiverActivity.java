package com.nuospin.virtual_idtransfer;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.nuospin.virtual_idtransfer.indicators.LineScalePulseOutIndicator;
import com.nuospin.virtual_idtransfer.process.Receiver;
import com.nuospin.virtual_idtransfer.widgets.AVLoadingIndicatorView;

public class ReceiverActivity extends AppCompatActivity {

    private static final String TAG = ReceiverActivity.class.getSimpleName();
    Context mContext;
    Toolbar mToolbar;
    AVLoadingIndicatorView avi;
    EditText et_receivedVirtualId;
    ImageView ivMic;
    private Receiver receiver;
    boolean flag = true;
    ReceiverActivity activityObj;
    private Vibrator v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiver);
        init();
        getViews();
    }

    private void init() {
        setTitle("");
        mContext = this;
        activityObj = this;
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
    }

    private void getViews(){
        et_receivedVirtualId = (EditText) findViewById(R.id.et_virtual_id);
        ivMic = (ImageView) findViewById(R.id.iv_mic);
        avi = (AVLoadingIndicatorView) findViewById(R.id.line_indicator);
        LineScalePulseOutIndicator lspli = new LineScalePulseOutIndicator();
        avi.setIndicator(lspli);
        avi.hide();
        ivMic.setOnClickListener(micClick);
    }

    private View.OnClickListener micClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(flag){
                et_receivedVirtualId.setText("");
                if (receiver == null || receiver.isCancelled()
                        || receiver.getStatus() == AsyncTask.Status.FINISHED) {
                    receiver = new Receiver(activityObj);
                    receiver.execute();
                    flag = false;
                    avi.smoothToShow();
                }else {
                    flag = true;
                    avi.smoothToHide();
                }
            }else {
                if(receiver.getStatus() == AsyncTask.Status.FINISHED){
                    flag = true;
                    avi.smoothToHide();
                }
                if (receiver.getStatus() != AsyncTask.Status.FINISHED) {
                    receiver.cancel(true);
                    avi.smoothToHide();
                    flag = true;
                }
            }

        }
    };

    public void finished(String result) {
        v.vibrate(500);
        avi.smoothToHide();
        flag = true;
        et_receivedVirtualId.setText(result.trim());
        hideSoftKeyboard();
    }

    private void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (imm.isAcceptingText()) {
            if (getCurrentFocus() != null)
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
