package com.nuospin.virtual_idtransfer;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nuospin.virtual_idtransfer.process.Generator;
import com.nuospin.virtual_idtransfer.widgets.AVLoadingIndicatorView;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    Context mContext;
    Toolbar mToolbar;
    AVLoadingIndicatorView avi;
    Button btnSend, btnReceive;
    TextView tvSendingText;
    EditText etVirtualId;
    TextInputLayout inputLayoutVirtualId;
    private Generator generator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        getViews();
    }

    private void init() {
        setTitle("");
        mContext = this;
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void getViews() {
        inputLayoutVirtualId = (TextInputLayout) findViewById(R.id.input_layout_virtual_id);
        etVirtualId = (EditText) findViewById(R.id.et_virtual_id);
        avi = (AVLoadingIndicatorView) findViewById(R.id.indicator);
        avi.hide();
        tvSendingText = (TextView) findViewById(R.id.tv_sending);
        btnSend = (Button) findViewById(R.id.btn_send);
        btnReceive = (Button) findViewById(R.id.btn_receive);
        btnSend.setOnClickListener(sendButtonClick);
        btnReceive.setOnClickListener(receiverButtonClick);
        //avi.setIndicator("BallScaleMultipleIndicator");
    }

    private View.OnClickListener sendButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (btnSend.getText().toString().equals(getString(R.string.txt_send)) && validateVirtualIdAndHandleIfEmpty(etVirtualId)) {
                if (generator == null || generator.isCancelled()) {
                    String virtual_id = etVirtualId.getText().toString();
                    byte[] arrayToSend = Arrays.copyOfRange(virtual_id.getBytes(), 0, 30);
                    generator = new Generator(arrayToSend);
                    generator.execute();
                    btnSend.setText(getString(R.string.txt_stop));
                    showAvi();
                    tvSendingText.setVisibility(View.VISIBLE);
                    hideSoftKeyboard();
                }
            } else {
                generator.cancel(true);
                btnSend.setText(getString(R.string.txt_send));
                hideAvi();
                tvSendingText.setVisibility(View.INVISIBLE);
            }
        }
    };

    private View.OnClickListener receiverButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(mContext, ReceiverActivity.class));
        }
    };

    public void hideAvi() {
        avi.smoothToHide();
    }

    public void showAvi() {
        avi.smoothToShow();
    }

    private boolean validateVirtualIdAndHandleIfEmpty(EditText editField) {
        if (editField.getText().toString().trim().length() >= 5) {
            inputLayoutVirtualId.setEnabled(false);
            inputLayoutVirtualId.setError(null);
            return true;
        }
        inputLayoutVirtualId.setError(getString(R.string.error_invalid_id));
        return false;
    }

    private void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (imm.isAcceptingText()) {
            if (getCurrentFocus() != null)
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
