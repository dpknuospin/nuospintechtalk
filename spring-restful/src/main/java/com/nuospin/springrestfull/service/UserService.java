package com.nuospin.springrestfull.service;

import com.nuospin.springrestfull.domain.User;
import com.nuospin.springrestfull.dto.UserDto;

public interface UserService {
	
	public User getUserByEmail(String email);
	
	public User saveUser( UserDto user);
	
	public User updateUser(String id, UserDto user);
	
	public void deleteUser(String id);
	
	public User getUserByFirstName(String firstName);

}
