/* lets create a person class and then show how to use the prototype to establish a parent child relationship(inheritance) among the person and employee*/


function Person () { 
this.Country = "INDIA"; 
this.isGraduated = true; 
} 

// Add the showNameAndAge method to the Person prototype property 
Person.prototype.showNameAndAge =  function () { 
console.log("I am " + this.name + " and my age is " + this.age); 
} 

// Add the amIGraduated method to the Person prototype property 
Person.prototype.amIGraduated = function () { 
if (this.isGrauated) 
console.log(" Hey! I am Graduated"); 
} 

function Employee (empName, empAge) { 
this.name = empName; 
this.age = empAge; 
} 

// Set the Employee's prototype to Person's constructor, thus inheriting
// all of Person.prototype methods and properties. 
Employee.prototype = new Person(); 

// Creates a new object, emp1, with the Employee constructor 
var emp1 = new Employee ("sachin", "24"); 

// In this case, emp1 uses the name property from the emp1 object prototype,
//which is Employee.prototype : 
console.log(emp1.name); // sachin 

// Now make the use of showNameAndAge method from the Employee object 
//prototype, which is Person.prototype. The emp1 object inherits all the 
//properties and methods from both the Employee and Person functions. 
console.log(emp1.showNameAndAge()); // I am sachin and my age is 24.
