/* lets create a sample employee class then perform different addition and deletion on the instances of this class and figure out the difference among normal property addition and through prototype*/

function Employee(name,age,address,email) {
	this.name= name;
	this.age=age;
	this.address=address;
	this.email=email;
} 

var emp1 = new Employee('sachin','24','Noida','sachin@nuospin.com');
var emp2 = new Employee('shashank','22','Delhi','shashank@nuospin.com');
var emp3 = new Employee('rahul','24','Gurugram','rahul@nuospin.com');

console.log("Employee 2 is "+ emp2.name+ " from " +emp2.address);

emp1.company='nuospin'; // company property is added to only emp1 instance of employee

console.log("Employee 1 is "+ emp1.name+ " from " +emp1.address + " working at " + emp1.company );
console.log("Employee 2 is "+ emp2.name+ " from " +emp2.address + " working at " + emp2.company );// in this case company property will not be accessible to emp2 

// making company property available to all the instances of employee with the help of constructor function
/*
function Employee(name,age,address,email) {
	this.name= name;
	this.age=age;
	this.address=address;
	this.email=email;
	this.company= 'nuospin';
	} 
*/
// making company property available to all the instances of employee with the help of prototype property
Employee.prototype.company='nuospin';


var emp4 = new Employee('satwinder','25','punjab','satwinder@nuospin.com');

console.log("Employee 4 is "+ emp4.name+ " from " +emp4.address + " working at " + emp4.company ); // here company property will be by default available to emp5 instance
console.log("Employee 2 is "+ emp2.name+ " from " +emp2.address + " working at " + emp2.company );//now companny property will also be available to old instances of employee
