package com.nuospin.springrestfull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@Profile("test")
public class ApplicationTest {
	
	@Autowired
	private MongoTemplate mongoTamplate;

	@Test
	public void contextLoads() {
	}
	
	@Before
	public void setup()  {
		// we can have a data builder method here
	}

	@After
	public void tearDown() {
		mongoTamplate.getDb().dropDatabase();
	}

}
